//
//  GlanceController.swift
//  sLick WatchKit Extension
//
//  Created by Krzysztof Rutkowski on 8/11/2015.
//  Copyright © 2015 Krzysztof Rutkowski. All rights reserved.
//

import WatchKit
import Foundation


class GlanceController: WKInterfaceController {

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
